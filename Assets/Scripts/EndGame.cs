﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndGame : MonoBehaviour
{
    public void Button_Click()
    {
        Debug.Log("Game Ended! The Application closes down, ending the game.");

        /*
        Application.Quit();
        */
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            Debug.Log("Game Ended! The Application closes down, ending the game.");

            /*
            Application.Quit();
            */
        }
    }

}
